package com.epam;

public class Fibonacci {
    private int quantity;
    private int[] array;

    public Fibonacci(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void Generation() {
        this.array = new int[quantity + 1];
        array[0] = 0;
        array[1] = 1;
        for (int i = 2; i < quantity + 1; i++) {
            array[i] = array[i - 1] + array[i - 2];
        }
    }

    public void MaxOddAndEven() {
        int MaxOdd = array[0];
        int MaxEven = array[0];
        for (int i = 1; i < quantity + 1; i++) {
            if (array[i] % 2 == 1 && array[i] > MaxOdd) {
                MaxOdd = array[i];
            }
            if (array[i] % 2 == 0 && array[i] > MaxEven) {
                MaxEven = array[i];
            }
        }
        System.out.println("The biggest odd number:" + MaxOdd);
        System.out.println("The biggest even number:" + MaxEven);
    }

    public void PrintPercentage() {
        double percentageOfOdd = 1;
        double percentageOfEven = 1;
        double quantityOfOdd = 0;
        double quantityOfEven = 0;
        for (int i = 1; i < quantity + 1; i++) {
            if (array[i] % 2 == 1) {
                quantityOfOdd++;
            }
            if (array[i] % 2 == 0) {
                quantityOfEven++;
            }
        }
        percentageOfOdd = (quantityOfOdd / (double) quantity) * 100;
        percentageOfEven = (quantityOfEven / (double) quantity) * 100;
        System.out.println("Percentage of odd numbers:" + percentageOfOdd + " %");
        System.out.println("Percentage of even numbers:" + percentageOfEven + " %");
    }

    public void Display() {
        System.out.print("The Fibonacci sequence:");
        for (int i = 1; i < quantity + 1; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
