package com.epam;

import java.util.Scanner;

public class Number {
    private int beginning;
    private int end;

    public Number() {
    }

    public Number(int beginning, int end) {
        this.beginning = beginning;
        this.end = end;
    }

    public int getBeginning() {
        return beginning;
    }

    public void setBeginning(int beginning) {
        this.beginning = beginning;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public void Read() {
        Scanner input = new Scanner(System.in);
        int number1;
        int number2;
        System.out.print("Enter the beginning of the interval:");
        number1 = input.nextInt();
        System.out.print("Enter the end of the interval:");
        number2 = input.nextInt();
        setBeginning(number1);
        setEnd(number2);
    }

    public void Print() {
        System.out.print("The succession of odd numbers:");
        for (int i = beginning; i <= end; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
        System.out.print("The succession of even numbers:");
        for (int i = end; i >= beginning; i--) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    public void Sum() {
        int odd = beginning;
        int even = beginning + 1;
        System.out.print("Sum of odd and even numbers:");
        while (even <= end || odd <= end) {
            if (even <= end && odd <= end) {
                System.out.print(odd + even + " ");
            } else if (even <= end && odd > end) {
                System.out.println(even + " ");
            } else {
                System.out.print(odd + " ");
            }
            odd += 2;
            even += 2;
        }
    }
}