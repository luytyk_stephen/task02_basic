package com.epam;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Number number = new Number();
        number.Read();
        number.Print();
        number.Sum();
        System.out.println();
        System.out.print("Enter the quantity of Fibonacci numbers:");
        Scanner input = new Scanner(System.in);
        int quantity = input.nextInt();
        Fibonacci fibonacci = new Fibonacci(quantity);
        fibonacci.Generation();
        fibonacci.Display();
        System.out.println();
        fibonacci.MaxOddAndEven();
        fibonacci.PrintPercentage();
    }
}
